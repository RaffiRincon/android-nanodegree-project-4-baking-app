package com.example.rafael.delicious;

import android.app.Activity;
import android.os.Looper;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.StringContains.containsString;

import	android.support.test.espresso.contrib.RecyclerViewActions;

@RunWith(AndroidJUnit4.class)
public class NavigationToRecipDetailTests {
	@Rule public ActivityTestRule<RecipeListActivity> activityActivityTestRule =
			new ActivityTestRule<>(RecipeListActivity.class);
	
	@Before
	public void init() {
		IdlingRegistry idlingRegistry = IdlingRegistry.getInstance();
		idlingRegistry.register(activityActivityTestRule.getActivity().getRecipesIdlingResource);
	}
	
	@Test
	public void testNavigationFromRecipeListActivity() {
		onView(ViewMatchers.withId(R.id.recipe_list))
				.perform(RecyclerViewActions.<RecipesAdapter.RecipeViewHolder>actionOnItemAtPosition(0, click()));
		IdlingRegistry.getInstance().register(activityActivityTestRule.getActivity().launchRecipeDetailActivityIdlingResource);
		onView(withId(R.id.tv_ingredients_title)).check(matches(isDisplayed()));
	}
	
	@Test
	public void testIngredientsStringLoadedProperly() {
		testNavigationFromRecipeListActivity();
		
		Activity activity = TestUtils.getActivityInstance();
		
		Recipe recipe = null;
		if (activity instanceof RecipeDetailActivity) {
			recipe = ((RecipeDetailActivity)activity).recipe;
		} else if (activity instanceof  RecipeListActivity) {
			RecipeDetailFragment recipeDetailFragment =
					(RecipeDetailFragment) ((RecipeListActivity) activity)
												   .getSupportFragmentManager()
												   .findFragmentById(R.id.recipe_detail_container);
			
			recipe = recipeDetailFragment.recipe;
		}
		
		onView(withId(R.id.fragment_recipe_detail_tv_ingredients))
				.check(matches(withText(containsString(recipe.ingredientsString()))));
	}
	
}
