package com.example.rafael.delicious;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class Ingredient implements Parcelable {
	public double quantity;
	public String unit;
	public String name;

	public Ingredient() {}

	protected Ingredient(Parcel in) {
		quantity = in.readDouble();
		unit = in.readString();
		name = in.readString();
	}

	public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
		@Override
		public Ingredient createFromParcel(Parcel in) {
			return new Ingredient(in);
		}

		@Override
		public Ingredient[] newArray(int size) {
			return new Ingredient[size];
		}
	};


	private static HashMap<String, String> prettyPluralUnitFromUnit = createPrettyPluralUnitFromUnitMap();

	private static HashMap<String, String> createPrettyPluralUnitFromUnitMap() {
		HashMap<String, String> result = new HashMap<>();

		result.put("UNIT", "");
		result.put("G", "grams ");
		result.put("TBLSP", "tablespoons ");
		result.put("TSP", "teaspoons ");
		result.put("CUP", "cups ");
		result.put("K", "kilograms ");
		result.put("OZ", "ounces ");

		return result;
	}

	private static HashMap<String, String> prettySingularUnitFromUnit = createPrettySingularUnitFromUnitMap();

	private static HashMap<String, String> createPrettySingularUnitFromUnitMap() {
		HashMap<String, String> result = new HashMap<>();

		result.put("UNIT", "");
		result.put("G", "gram ");
		result.put("TBLSP", "tablespoon ");
		result.put("TSP", "teaspoon ");
		result.put("CUP", "cup ");
		result.put("K", "kilogram ");
		result.put("OZ", "ounce ");

		return result;
	}



	@Override
	public String toString() {
		String prettyUnit = unit + " ";
		if (quantity == 1) {
			prettyUnit = prettySingularUnitFromUnit.get(unit);
		} else {
			prettyUnit = prettyPluralUnitFromUnit.get(unit);
		}

		String prettyQuantity;
		if (quantity == ((double)(int) (quantity))) {
			prettyQuantity = ((int) quantity) + "";
		} else {
			prettyQuantity = quantity + "";
		}

		StringBuilder sb = new StringBuilder()
				.append(prettyQuantity)
				.append(" ")
				.append(prettyUnit)
				.append(name);

		return sb.toString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeDouble(quantity);
		parcel.writeString(unit);
		parcel.writeString(name);
	}
}
