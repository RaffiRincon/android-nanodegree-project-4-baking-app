package com.example.rafael.delicious;

import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class JSONUtils {

	private static final String TAG = JSONUtils.class.getName();

	@Nullable
	static List<Recipe> recipesFromJSONString(String jsonString) {
		if (jsonString == null) {
			return null;
		}

		List<Recipe> recipes = null;
		try {
			JSONArray recipesJSON = new JSONArray(jsonString);

			recipes = new ArrayList<>();
			for (int i = 0; i < recipesJSON.length(); i++) {
				JSONObject recipeJSON = recipesJSON.getJSONObject(i);
				recipes.add(recipeFromJSON(recipeJSON));
			}

			recipes.sort(new Comparator<Recipe>() {
				@Override
				public int compare(Recipe recipe, Recipe t1) {
					return  recipe.id - t1.id;
				}
			});

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return recipes;
	}

	private static Recipe recipeFromJSON(JSONObject recipeJSON) {
		Recipe recipe = new Recipe();
		try {
			recipe.id = recipeJSON.getInt("id");
			recipe.imageURL = recipeJSON.getString("image");
			recipe.servings = recipeJSON.getInt("servings");
			recipe.name = recipeJSON.getString("name");

			JSONArray ingredientsJSON = recipeJSON.getJSONArray("ingredients");
			recipe.ingredients = new ArrayList<>();
			for (int i = 0; i < ingredientsJSON.length(); i++) {
				JSONObject ingredientJSON = ingredientsJSON.getJSONObject(i);
				recipe.ingredients.add(ingredientFromJSON(ingredientJSON));
			}

			JSONArray steps = recipeJSON.getJSONArray("steps");
			recipe.steps = new ArrayList<>();
			for (int i = 0; i < steps.length(); i++) {
				JSONObject stepJSON = steps.getJSONObject(i);
				recipe.steps.add(stepFromJSON(stepJSON));
			}

			recipe.steps.sort(new Comparator<Step>() {
				@Override
				public int compare(Step step, Step t1) {
					return step.number - t1.number;
				}
			});

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return recipe;
	}


	private static Ingredient ingredientFromJSON(JSONObject ingredientJSON) {
		Ingredient ingredient = new Ingredient();
		try {
			ingredient.quantity = ingredientJSON.getDouble("quantity");
			ingredient.unit = ingredientJSON.getString("measure");
			ingredient.name = ingredientJSON.getString("ingredient");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return ingredient;
	}

	private static Step stepFromJSON(JSONObject stepJSON) {
		Step step = null;

		try {
			step = new Step();
			step.number = stepJSON.getInt("id");
			step.description = stepJSON.getString("description");
			step.shortDescription = stepJSON.getString("shortDescription");
			step.thumbnailUrlString = stepJSON.getString("thumbnailURL");
			step.videoUrlString = stepJSON.getString("videoURL");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return step;
	}

}
