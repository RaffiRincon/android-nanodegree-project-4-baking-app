package com.example.rafael.delicious;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

class NetworkUtils {

	private static String TAG = NetworkUtils.class.getName();
	private static final String recipesURL = "https://d17h27t6h515a5.cloudfront.net/topher/2017/May/59121517_baking/baking.json";

	@Nullable
	private static String getResponse(URL url, Context context) {
		if (!deviceIsConnectedToNetwork(context)) {
			return null;
		}

		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			try {
				InputStream responseStream = connection.getInputStream();
				Scanner scanner = new Scanner(responseStream);
				scanner.useDelimiter("\\A");

				if (scanner.hasNext()) {
					return scanner.next();
				} else {
					return null;
				}
			} catch (IOException e) {
				e.printStackTrace();
				Log.e(TAG, "could not get input stream from connection " + connection);
			} finally {
				connection.disconnect();
			}
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "error creating HttpConnection with URL " + url.toString());
		}

		return null;
	}

	@Nullable
	static List<Recipe> getRecipes(Context context) {
		try {
			URL url = new URL(recipesURL);
			String response = getResponse(url, context);
			return JSONUtils.recipesFromJSONString(response);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return null;
	}

	static boolean deviceIsConnectedToNetwork(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetworkInfo = cm == null? null : cm.getActiveNetworkInfo();

		return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}
	
	static boolean isValidAsURL(Uri uri) {
		try {
			new URL(uri.toString());
			return true;
		} catch (MalformedURLException e) {
			return false;
		}
	}
}
