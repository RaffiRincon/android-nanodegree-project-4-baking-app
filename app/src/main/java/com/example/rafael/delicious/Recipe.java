package com.example.rafael.delicious;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Recipe implements Parcelable {

	public int id;
	int servings;
	String imageURL;
	public String name;

	List<Step> steps;
	List<Ingredient> ingredients;

	public Recipe() { }

	protected Recipe(Parcel in) {
		id = in.readInt();
		servings = in.readInt();
		imageURL = in.readString();
		name = in.readString();
		steps = new ArrayList<>();
		in.readTypedList(steps, Step.CREATOR);
		ingredients = new ArrayList<>();
		in.readTypedList(ingredients, Ingredient.CREATOR);
	}

	public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
		@Override
		public Recipe createFromParcel(Parcel in) {
			return new Recipe(in);
		}

		@Override
		public Recipe[] newArray(int size) {
			return new Recipe[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(id);
		parcel.writeInt(servings);
		parcel.writeString(imageURL);
		parcel.writeString(name);
		parcel.writeTypedList(steps);
		parcel.writeTypedList(ingredients);
	}
	
	String ingredientsString() {
		StringBuilder builder = new StringBuilder();
		for (Ingredient ingredient : ingredients) {
			builder.append(ingredient.toString()).append("\n");
		}
		
		builder.delete(builder.length() - 1, builder.length());
		
		return builder.toString();
	}
}
