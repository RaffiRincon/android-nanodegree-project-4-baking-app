package com.example.rafael.delicious;

import android.os.Bundle;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

/**
 * An activity representing a single Recipe detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link RecipeListActivity}.
 */
public class RecipeDetailActivity extends AppCompatActivity {

	static final String RECIPE_KEY = "recipe";
	Recipe recipe;
	CountingIdlingResource loadRecipeResource = new CountingIdlingResource("loadRecipeResource");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recipe_detail);

		setSupportActionBar((Toolbar) findViewById(R.id.activity_recipe_detail_toolbar));
		// Show the Up button in the action bar.
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			recipe = getIntent().getParcelableExtra(RECIPE_KEY);
			setTitle(recipe.name);

			arguments.putParcelable(RecipeDetailFragment.RECIPE_KEY, recipe);
			RecipeDetailFragment fragment = new RecipeDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.recipe_detail_container, fragment)
					.commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			FragmentManager manager = getSupportFragmentManager();
			if (manager.findFragmentById(R.id.recipe_detail_container) instanceof RecipeDetailFragment) {
				NavUtils.navigateUpFromSameTask(this);
			} else {
				getSupportFragmentManager().popBackStackImmediate();
			}

			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
