package com.example.rafael.delicious;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A fragment representing a single Recipe detail screen.
 * This fragment is either contained in a {@link RecipeListActivity}
 * in two-pane mode (on tablets) or a {@link RecipeDetailActivity}
 * on handsets.
 */
public class RecipeDetailFragment extends Fragment {
	/**
	 * The key to put and get the recipe for this fragment into and out of the arguments Bundle.
	 */
	public static final String RECIPE_KEY = "recipe";

	Recipe recipe;
	private boolean isTwoPane = false;

	public RecipeDetailFragment() {}
	
	@BindView(R.id.rv_steps) RecyclerView stepRV;
	@BindView(R.id.fragment_recipe_detail_tv_ingredients) TextView ingredientsTV;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		FragmentActivity activity = getActivity();
		if(activity instanceof RecipeListActivity) {
			isTwoPane = ((RecipeListActivity) getActivity()).isTwoPane;
		}

		View rootView = inflater.inflate(R.layout.fragment_recipe_detail, container, false);
		ButterKnife.bind(this, rootView);

		if (getArguments() == null || !getArguments().containsKey(RECIPE_KEY)) {
			return rootView;
		} else {
			recipe = (Recipe) getArguments().get(RECIPE_KEY);
			populateUI();
			return rootView;
		}
	}

	private void configureRecyclerView() {
		stepRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
		StepAdapter adapter = new StepAdapter(recipe.steps);
		adapter.parentActivity = getActivity();
		stepRV.setAdapter(adapter);
	}

	private void populateToolbar() {
		if (isTwoPane) {
			return;
		}

		AppCompatActivity parentActivity = (AppCompatActivity) getActivity();
		ActionBar actionBar = parentActivity == null ? null : parentActivity.getSupportActionBar();
		
		if (actionBar == null) {
			return;
		}
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(recipe.name);
	}

	private void populateIngredientsTextView() {
		ingredientsTV.setText(recipe.ingredientsString());
	}

	private void populateUI() {
		populateToolbar();
		populateIngredientsTextView();
		configureRecyclerView();
	}
}
