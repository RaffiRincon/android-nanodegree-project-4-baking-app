package com.example.rafael.delicious;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An activity representing a list of Recipes. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link RecipeDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class RecipeListActivity extends AppCompatActivity {
	
	
	CountingIdlingResource getRecipesIdlingResource = new CountingIdlingResource("getRecipesIdlingResource");
	CountingIdlingResource launchRecipeDetailActivityIdlingResource =
			new CountingIdlingResource("launchRecipeDetailActivityIdlingResource");
	
	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	public boolean isTwoPane;
	
	@BindView(R.id.recipe_list) RecyclerView recipeList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(this.getClass().getName(), "intent: " + getIntent().toString());
		setContentView(R.layout.activity_recipe_list);
		ButterKnife.bind(this);
		launchRecipeDetailActivityIdlingResource.increment();

		View container = findViewById(R.id.recipe_detail_container);
		
		if (container != null) {
			isTwoPane = true;
		}
		recipeList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

		GetRecipesTask getRecipesTask = new GetRecipesTask(this);
		getRecipesTask.execute();
	}

	// oh yeah, no warnings!
	static  class GetRecipesTask extends AsyncTask<Void, Void, List<Recipe>> {

		private WeakReference<RecipeListActivity> activity;

		GetRecipesTask(RecipeListActivity activity) {
			this.activity = new WeakReference<>(activity);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			activity.get().getRecipesIdlingResource.increment();
		}
		
		@Override
		protected List<Recipe> doInBackground(Void... voids) {
			return NetworkUtils.getRecipes(activity.get());
		}

		@Override
		protected void onPostExecute(List<Recipe> recipes) {
			if (recipes == null) {
				Toast.makeText(
						activity.get(),
						activity.get().getString(R.string.no_internet_try_again_message),
						Toast.LENGTH_SHORT).show();
			} else {
				
				RecipesAdapter recipesAdapter = new RecipesAdapter(recipes, activity.get());
				activity.get().recipeList.setAdapter(recipesAdapter);
			}
			activity.get().getRecipesIdlingResource.decrement();
		}
	}
}
