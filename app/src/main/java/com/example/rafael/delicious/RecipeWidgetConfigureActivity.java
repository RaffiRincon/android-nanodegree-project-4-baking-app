package com.example.rafael.delicious;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The configuration screen for the {@link RecipeWidgetProvider RecipeWidgetProvider} AppWidget.
 */
public class RecipeWidgetConfigureActivity extends Activity {
	
	final static String INGREDIENTS_PREF_KEY_PREFIX = "ingredients_widget_id_";
	final static String SHARED_PREFS_NAME = "com.example.rafael.delicious.RecipeWidgetConfigureActivity";
	
	int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
	Recipe recipeForWidget = null;
	
	@BindView(R.id.add_button) Button addButton;
	
	RecyclerView recyclerView;
	View.OnClickListener mOnClickListener = new View.OnClickListener() {
		public void onClick(View v) {
			final Context context = RecipeWidgetConfigureActivity.this;
			
			// When the button is clicked, store the string locally
			saveRecipeIngredientsString(context, recipeForWidget, appWidgetId);
			
			// It is the responsibility of the configuration activity to update the app widget
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			RecipeWidgetProvider.updateAppWidget(context, appWidgetManager, appWidgetId);
			
			// Make sure we pass back the original appWidgetId
			Intent resultValue = new Intent();
			resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			setResult(RESULT_OK, resultValue);
			finish();

			Log.d(context.getClass().getName(), "called addbutton onclick listener");
		}
	};

	public RecipeWidgetConfigureActivity() {
		super();
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		// Set the result to CANCELED.  This will cause the widget host to cancel
		// out of the widget placement if the user presses the back button.
		setResult(RESULT_CANCELED);

		setContentView(R.layout.recipe_widget_configure);
		
		ButterKnife.bind(this);
		
		findViewById(R.id.add_button).setOnClickListener(mOnClickListener);

		recyclerView = findViewById(R.id.rv_recipes_widget_config);
		recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		new GetRecipesTask(this).execute();

		// Find the widget id from the intent.
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
			appWidgetId = extras.getInt(
					AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		}

		// If this activity was started with an intent without an app widget ID, finish with an error.
		if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
			finish();
		}
	}


	// oh yeah, no warnings!
	static class GetRecipesTask extends AsyncTask<Void, Void, List<Recipe>> {

		private WeakReference<RecipeWidgetConfigureActivity> activity;

		GetRecipesTask(RecipeWidgetConfigureActivity activity) {
			this.activity = new WeakReference<>(activity);
		}

		@Override
		protected List<Recipe> doInBackground(Void... voids) {
			return NetworkUtils.getRecipes(activity.get());
		}

		@Override
		protected void onPostExecute(List<Recipe> recipes) {
			if (recipes == null) {
				Toast.makeText(activity.get(), activity.get().getString(R.string.no_internet_try_again_message), Toast.LENGTH_SHORT).show();
				return;
			}
			
			WidgetConfigureRecipesAdapter recipesAdapter = new WidgetConfigureRecipesAdapter(recipes, activity.get());

			activity.get().recyclerView.setAdapter(recipesAdapter);
		}
	}
	
	static void saveRecipeIngredientsString(Context context, Recipe recipe, int appWidgetId) {
		if (recipe == null) {
			Log.d(context.getClass().getName(), "tried to save ingredients string to shared prefs with null recipeForWidget");
			return;
		}
		SharedPreferences.Editor editor = context.getSharedPreferences(SHARED_PREFS_NAME, 0).edit();
		editor.putString(INGREDIENTS_PREF_KEY_PREFIX + appWidgetId, recipe.ingredientsString());
		editor.apply();
	}
	
	
	static String loadRecipeIngredientString(Context context, int appWidgetId) {
		SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS_NAME, 0);
		String ingredientsString = prefs.getString(INGREDIENTS_PREF_KEY_PREFIX + appWidgetId, null);
		return ingredientsString;
	}
}

