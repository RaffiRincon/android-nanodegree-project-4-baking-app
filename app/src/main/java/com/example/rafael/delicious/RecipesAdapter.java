package com.example.rafael.delicious;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.RecipeViewHolder> {

	private List<Recipe> recipes;
	private RecipeListActivity parentActivity;

	RecipesAdapter(List<Recipe> recipes, RecipeListActivity parentActivity) {
		super();

		this.recipes = recipes;
		this.parentActivity = parentActivity;
	}

	@NonNull
	@Override
	public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		LayoutInflater inflater = LayoutInflater.from(parentActivity);
		View recipeView = inflater.inflate(R.layout.recipe_list_content, viewGroup, false);
		RecipeViewHolder recipeViewHolder = new RecipeViewHolder(recipeView);
		return recipeViewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull RecipeViewHolder recipeViewHolder, int i) {
		recipeViewHolder.titleTV.setText(recipes.get(i).name);
		final Recipe recipe = recipes.get(i);

		View recipeView = recipeViewHolder.itemView;

		recipeView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (parentActivity.isTwoPane) {
					RecipeDetailFragment detailFragment = new RecipeDetailFragment();

					Bundle arguments = new Bundle();
					arguments.putParcelable(RecipeDetailFragment.RECIPE_KEY, recipe);
					detailFragment.setArguments(arguments);

					parentActivity.getSupportFragmentManager().beginTransaction()
							.replace(R.id.recipe_detail_container, detailFragment)
							.commit();
				} else {
					Intent startRecipeDetailActivity = new Intent(parentActivity, RecipeDetailActivity.class);
					startRecipeDetailActivity.putExtra(RecipeDetailActivity.RECIPE_KEY, recipe);
					parentActivity.startActivity(startRecipeDetailActivity);
					Log.d(getClass().getName(), "launched RecipeDetailActivity");
				}
				
				parentActivity.launchRecipeDetailActivityIdlingResource.decrement();
			}
		});
	}


	@Override
	public int getItemCount() {
		if (recipes == null) {
			return 0;
		} else {
			return recipes.size();
		}
	}

	class RecipeViewHolder extends RecyclerView.ViewHolder {
		TextView titleTV;

		RecipeViewHolder(@NonNull View itemView) {
			super(itemView);

			titleTV = itemView.findViewById(R.id.tv_title);
		}
	}
}
