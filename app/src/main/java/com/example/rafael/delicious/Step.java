package com.example.rafael.delicious;

import android.os.Parcel;
import android.os.Parcelable;

public class Step implements Parcelable {
	public int number;
	String shortDescription;
	public String description;

	String videoUrlString;
	String thumbnailUrlString;

	Step() {}

	private Step(Parcel in) {
		number = in.readInt();
		shortDescription = in.readString();
		description = in.readString();
		videoUrlString = in.readString();
		thumbnailUrlString = in.readString();
	}

	public static final Creator<Step> CREATOR = new Creator<Step>() {
		@Override
		public Step createFromParcel(Parcel in) {
			return new Step(in);
		}

		@Override
		public Step[] newArray(int size) {
			return new Step[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(number);
		parcel.writeString(shortDescription);
		parcel.writeString(description);
		parcel.writeString(videoUrlString);
		parcel.writeString(thumbnailUrlString);
	}
}
