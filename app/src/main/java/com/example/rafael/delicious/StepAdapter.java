package com.example.rafael.delicious;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.StepViewHolder> {

	private List<Step> steps;

	FragmentActivity parentActivity;

	StepAdapter(List<Step> steps) {
		this.steps = steps;
	}

	@NonNull
	@Override
	public StepViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
		View stepView = inflater.inflate(R.layout.step_view_for_rv, viewGroup, false);

		return new StepViewHolder(stepView);
	}

	@Override
	public void onBindViewHolder(@NonNull final StepViewHolder stepViewHolder, int i) {
		final Step step = steps.get(i);

		stepViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				StepDetailFragment stepFragment = new StepDetailFragment();
				stepFragment.step = step;

				FragmentManager fragmentManager = parentActivity.getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.recipe_detail_container, stepFragment)
						.addToBackStack(null)
						.commit();
			}
		});

		stepViewHolder.titleTV.setText(step.shortDescription);
	}

	@Override
	public int getItemCount() {
		if (steps == null) {
			return 0;
		} else {
			return  steps.size();
		}
	}

	class StepViewHolder extends RecyclerView.ViewHolder {

		TextView titleTV;

		StepViewHolder(@NonNull View itemView) {
			super(itemView);
			titleTV = itemView.findViewById(R.id.tv_step_title);
		}
	}
}
