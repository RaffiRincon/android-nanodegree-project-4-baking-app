package com.example.rafael.delicious;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.rafael.delicious.NetworkUtils.isValidAsURL;

public class StepDetailFragment extends Fragment {

	@BindView(R.id.playerView) PlayerView playerView;
	private ExoPlayer player;
	Step step;
	private boolean isTwoPane = false;

	private final String stepKeyForInstanceState = "step";
	private final String positionKeyForInstanceState = "position";
	private final String playWhenReadyKeyForInstanceState = "playWhenReady";
	private long playbackPosition = 0L;
	private int currentWindowIndex = 0;
	private boolean playWhenReady;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		AppCompatActivity activity = (AppCompatActivity) getActivity();
		
		if (activity instanceof RecipeListActivity) {
			isTwoPane = ((RecipeListActivity)activity).isTwoPane;
		}

		loadStep(savedInstanceState);
		setPlayerStateFields(savedInstanceState);

		View rootView = inflater.inflate(R.layout.fragment_step_detail, container, false);
		ButterKnife.bind(this, rootView);
		
		initializePlayer();
		populateUI(rootView);

		return rootView;
	}
	
	private void setPlayerStateFields(@Nullable Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
		// defaults to 0L
		playbackPosition = savedInstanceState.getLong(positionKeyForInstanceState);
		playWhenReady = savedInstanceState.getBoolean(playWhenReadyKeyForInstanceState);
	}
	
	private void loadStep(@Nullable Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
		Step savedStep = savedInstanceState.getParcelable(stepKeyForInstanceState);
		if (savedStep != null) {
			step = savedStep;
		}
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		outState.putParcelable(stepKeyForInstanceState, step);

		if (player != null) {
			outState.putLong(positionKeyForInstanceState, player.getCurrentPosition());
			outState.putBoolean(playWhenReadyKeyForInstanceState, player.getPlayWhenReady());
		}

		super.onSaveInstanceState(outState);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		initializePlayer();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		// release player unconditionally since minSDKversion = 27
		releasePlayer();
	}
	
	private void populateUI(View rootView) {
		if (!isTwoPane && getActivity() != null ) {
			Toolbar toolbar = getActivity().findViewById(R.id.activity_recipe_detail_toolbar);
			if (toolbar != null) {
				toolbar.setTitle(step.shortDescription);
			}
		}
		
		TextView stepDescription = rootView.findViewById(R.id.tv_step_description);
		
		stepDescription.setText(step.description);
	}

	private void initializePlayer() {
		if (!NetworkUtils.deviceIsConnectedToNetwork(getActivity())) {
			Toast.makeText(getActivity(), getActivity().getString(R.string.no_internet_try_again_message), Toast.LENGTH_SHORT).show();
			return;
		}
		
		Uri stepVideoUri = new Uri.Builder().encodedPath(step.videoUrlString).build();
		if (!isValidAsURL(stepVideoUri)) {
			return;
		}
		
		TrackSelector trackSelector = new DefaultTrackSelector();
		player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
		playerView.setPlayer(player);

		String userAgent = Util.getUserAgent(getActivity(), getString(R.string.app_name));
		MediaSource mediaSource = new ExtractorMediaSource
				.Factory(new DefaultHttpDataSourceFactory(userAgent, null))
				.createMediaSource(stepVideoUri);
		
		player.prepare(mediaSource);
		player.seekTo(currentWindowIndex, playbackPosition);
		player.setPlayWhenReady(playWhenReady);
	}
	
	private void releasePlayer() {
		if (player != null) {
			playbackPosition = player.getCurrentPosition();
			currentWindowIndex = player.getCurrentWindowIndex();
			playWhenReady = player.getPlayWhenReady();
			player.release();
			player = null;
		}
	}
}
