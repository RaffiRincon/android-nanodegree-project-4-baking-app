package com.example.rafael.delicious;


import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;


public class WidgetConfigureRecipesAdapter extends RecyclerView.Adapter<WidgetConfigureRecipesAdapter.RecipeViewHolder> {
	private List<Recipe> recipes;

	RecipeWidgetConfigureActivity activity;

	public WidgetConfigureRecipesAdapter(List<Recipe> recipes, RecipeWidgetConfigureActivity activity) {
		this.recipes = recipes;
		this.activity = activity;
	}
	@NonNull
	@Override
	public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View recipeView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recipe_list_content, viewGroup, false);
		RecipeViewHolder viewHolder = new RecipeViewHolder(recipeView);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull RecipeViewHolder recipeViewHolder, int i) {
		recipeViewHolder.titleTextView.setText(recipes.get(i).name);
		final Recipe recipe = recipes.get(i);

		final Resources resources = activity.getResources();

		recipeViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				activity.recipeForWidget = recipe;
				Button addButton = activity.findViewById(R.id.add_button);
				addButton.setEnabled(true);
				view.setBackgroundColor(resources.getColor(R.color.colorAccent, resources.newTheme()));
			}
		});
	}

	@Override
	public int getItemCount() {
		if (recipes == null) {
			return 0;
		}
		return recipes.size();
	}

	class RecipeViewHolder extends RecyclerView.ViewHolder {
		TextView titleTextView;

		public RecipeViewHolder(@NonNull View itemView) {
			super(itemView);
			titleTextView = itemView.findViewById(R.id.tv_title);
		}
	}
}
